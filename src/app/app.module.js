"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
//imports
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var app_routing_1 = require("./routes/app.routing");

var ng2_ckeditor_1 = require("ng2-ckeditor");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");

//main components
var app_component_1 = require("./components/app/app.component");
var home_component_1 = require("./components/home/home.component");
var login_component_1 = require("./components/login/login.component");
var documentation_component_1 = require("./components/documentation/documentation.component");
var app_manager_component_1 = require("./components/app-manager/app.manager.component");
var login_container_component_1 = require("./components/login-container/login.container.component");

var news_manager_component_1 = require("./components/app-manager/news-manager/news.manager.component");

//shared
var spinner_component_1 = require("./shared/spinner/spinner.component");
//providers
var auth_service_1 = require("./services/auth.service");
var auth_guard_1 = require("./authentication/auth.guard");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
} ());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            app_routing_1.routing,
            forms_1.FormsModule,
            ng2_ckeditor_1.CKEditorModule,
            ng_bootstrap_1.NgbModule
        ],
        declarations: [
            //main components
            app_component_1.AppComponent,
            documentation_component_1.DocumentationComponent,
            app_manager_component_1.AppManagerComponent,
            home_component_1.HomeComponent,
            login_component_1.LoginComponent,
            login_container_component_1.LoginContainerComponent,

            news_manager_component_1.newsManagerComponent,

            //shared
            spinner_component_1.SpinnerComponent,
        ],
        bootstrap: [login_container_component_1.LoginContainerComponent],
        providers: [auth_service_1.Auth, auth_guard_1.AuthGuard]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map