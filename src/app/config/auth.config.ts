interface AuthConfiguration {
    clientID: string;
    domain: string;
}

export const AuthConfig: AuthConfiguration = {
    clientID: 'xOn8c7fYA9FgGb1tQlyFCbSJhQS51ocL',
    domain: '2rismapps.eu.auth0.com'
};
