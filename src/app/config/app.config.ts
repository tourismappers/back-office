export const AppConfig = {
    lang: 'es',
    city: 'valderrobres',
    API_PATH: {
        news: 'https:localhost:5000/tourismapps/api/v1',
        images: 'https:localhost:4000/tourismapps/api/v1/images/'
    },
    supportedLang: ['es', 'en', 'ca'],
    modules: [
        { key: 'dashboard', path: 'dashboard' },
        { key: 'news', path: 'news' },
        { key: 'images', path: 'images' },
        { key: 'sandbox', path: 'sandbox' }
    ],
    icons: [
        { key: 'settings', path: 'settings' },
        { key: 'message', path: 'message' },
        { key: 'notifications', path: 'notifications' },
        { key: 'power_settings_new', path: 'power_settings_new' }
    ]
};
