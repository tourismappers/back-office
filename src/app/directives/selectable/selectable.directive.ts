import { Directive, ElementRef, Renderer, HostListener, Input, AfterContentInit } from '@angular/core';

@Directive({
    selector: '[selectable]'
})

export class SelectableDirective implements AfterContentInit {
    @Input() initValue;
    private active: boolean;

    constructor(public el: ElementRef, public renderer: Renderer) {
    }

    ngAfterContentInit() {
        this.active = (this.initValue === 'true');
        this.renderer.setElementClass(this.el.nativeElement, 'active', this.active);
    }

    @HostListener('click', ['$event'])
    public onClick($event: Event) {
        this.active = !this.active;
        this.renderer.setElementClass(this.el.nativeElement, 'active', this.active);
    }

}