import { NgModule } from '@angular/core';

// Modulo que contiene todas las inyecciones compartidas para la aplicacion
import { SharedModule } from './core/shared/shared.module';

// modules components
import { NewsModule } from './modules/news-manager/news.manager.module';
import { ImagesModule } from './modules/images/images.module';


// core components
import { LoginModule } from './core/login/login.module';
import { AppManagerModule } from './core/app-manager/app.manager.module';
import { RootComponent } from './core/root.component';
import { NotFoundComponent } from './core/not-found/not-found.component';
import { SandboxModule } from './core/sandbox/sandbox.module';

// providers
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { AuthProvider } from './core/auth/auth.provider';


export function authHttpServiceFactory(http: Http, options: RequestOptions) {
}

@NgModule({
    imports: [
        // modules components
        NewsModule,
        ImagesModule,

        // core components
        SharedModule,
        AppManagerModule,
        LoginModule,
        SandboxModule,

        // others
        HttpModule,
    ],
    declarations: [
        // core components
        RootComponent,
        NotFoundComponent,
    ],
    bootstrap: [RootComponent],
    providers: [
        AuthProvider
    ],
})

export class AppModule { }
