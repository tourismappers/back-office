export const KEY_CODE = {
    ENTER: 13,
    TAB: 9,
    DOWN_ARROW: 40,
    UP_ARROW: 38
};