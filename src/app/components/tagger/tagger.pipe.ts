import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'autocompleteTags',
})
export class AutocompleTagsPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
        if (!items) {
            return [];
        } else if (value === '' || value === undefined) {
            return items;
        } else {
            return items.filter(newItem => newItem[field].includes(value));
        };
    }
}