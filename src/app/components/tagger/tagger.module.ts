import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { TaggerComponent } from './tagger.component';
import { TaggerProvider } from './tagger.provider';
import { AutocompleTagsPipe } from './tagger.pipe';
@NgModule({
    declarations: [
        TaggerComponent,
        AutocompleTagsPipe
    ],
    imports: [BrowserModule, FormsModule],
    exports: [TaggerComponent],
    providers: [TaggerProvider]
})
export class TaggerModule { }