import { Component, OnInit, HostListener, Output, EventEmitter, ViewChildren, Renderer2 } from '@angular/core';
import { TaggerProvider } from './tagger.provider';
import { KEY_CODE } from './tagger.constants';

@Component({
    moduleId: module.id,
    selector: 'tagger',
    templateUrl: 'tagger.component.html',
    styleUrls: ['tagger.component.scss']
})

export class TaggerComponent implements OnInit {
    @Output() getTag = new EventEmitter<any>();
    @ViewChildren('queryResult') queryResultList;

    private tagsArray: any;
    private query: any;
    private count = -1;
    constructor(private taggerProvider: TaggerProvider, private render: Renderer2) {
    }

    ngOnInit() {
        this.taggerProvider.getTags().subscribe(data => {
            this.tagsArray = data;
        }, error => {
            console.log(error);
        });
    }
    @HostListener('keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        event.preventDefault();
        switch (event.keyCode) {
            case KEY_CODE.ENTER:
                this.getInputValue();
                break;
            case KEY_CODE.UP_ARROW:
                this.selectDropdownValue(-1);
                break;
            case KEY_CODE.DOWN_ARROW:
                this.selectDropdownValue(1);
                break;
        }
    }
    getInputValue() {
        if (this.count !== -1) {
            this.getTag.emit(this.tagsArray[this.count - 1]);
        } else {
            this.getTag.emit({ id: new Date().getSeconds, text: this.query });
        }
        this.count = -1;
        this.query = '';
    }
    selectTag(tag) {
        this.query = '';
        this.getTag.emit(tag);
    }
    selectDropdownValue(index) {
        const results = this.queryResultList._results;
        this.count += index;
        if (results.length) {
            results.map((item, ind) => {
                switch (this.count) {
                    case ind:
                        this.render.setStyle(item.nativeElement, 'background', '#f2f2f2');
                        break;
                    case -1:
                        this.count = - 1;
                        this.render.setStyle(item.nativeElement, 'background', 'transparent');
                        break;
                    case -2:
                        this.count = results.length - 1;
                        this.render.setStyle(item.nativeElement, 'background', 'transparent');
                        break;
                    case results.length + 1:
                        this.count = -1
                        this.render.setStyle(item.nativeElement, 'background', 'transparent');
                        break;
                    case results.length:
                        this.count = 0
                        this.render.setStyle(item.nativeElement, 'background', '#f2f2f2');
                        break;
                    default:
                        this.render.setStyle(item.nativeElement, 'background', 'transparent');
                        break;
                }
            });
        };
    }
}

