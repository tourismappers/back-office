import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../config/app.config';

@Injectable()
export class TaggerProvider {
    private getTagsUrl: string;
    private tagList = [
        {
            _id: '143652f2wecdfg24',
            text: 'weather'
        },
        {
            _id: '134cyh24d5f6ydts',
            text: 'sports'
        },
        {
            _id: 'd345yd245d6fd345',
            text: 'noticias'
        },
        {
            _id: '134s51t345ds134q',
            text: 'messi'
        },
        {
            _id: 'd1451345f1d34m5',
            text: 'solde'
        },
    ];
    constructor(private http: Http) {
        this.getTagsUrl = AppConfig.API_PATH.news + '/categories';
    }

    getTags(): Observable<Array<any>> {
        return new Observable<Array<any>>(observer => {
            this.http.get(this.getTagsUrl).subscribe(data => {
                observer.next(this.tagList);
            }, error => {
                observer.error(error);
            });
        });
    }

}