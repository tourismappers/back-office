import { Component, OnInit, Input, Output, EventEmitter, Renderer, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'mobile-case',
    templateUrl: './mobile-case.html',
    styleUrls: ['./mobile-case.scss']
})
export class MobileCaseComponent implements OnInit {
    @ViewChild('content') content: ElementRef;
    @Input() background: string;
    private time = new Date();
    private newHide: boolean;

    constructor(private render: Renderer) { }

    ngOnInit() {
        if (this.background) {
            this.render.setElementStyle(this.content.nativeElement, 'background', `url(${this.background}) no-repeat`);
            this.render.setElementStyle(this.content.nativeElement, 'background-size', 'cover');
            this.render.setElementStyle(this.content.nativeElement, 'background-position', 'center');
        }
        this.timmer();
        this.newHide = false;
    }

    timmer() {
        setInterval(() => {
            this.time = new Date();
        }, 10000);
    }
}