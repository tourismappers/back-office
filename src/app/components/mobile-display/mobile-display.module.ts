import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MobileCaseComponent } from './mobile-case/mobile-case.component';

@NgModule({
    declarations: [MobileCaseComponent],
    imports: [BrowserModule],
    exports: [MobileCaseComponent],
})
export class MobileDisplayModule { }