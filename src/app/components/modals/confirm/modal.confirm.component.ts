import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
    selector: 'modal-confirm',
    templateUrl: 'modal.confirm.html',
    styleUrls: ['modal.confirm.scss']
})
export class DialogConfirmComponent {
    static data: any;
    private question: any;
    constructor(public dialogRef: MdDialogRef<DialogConfirmComponent>) {
        this.question = DialogConfirmComponent.data;
    }
}