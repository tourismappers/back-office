import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
    selector: 'modal-error',
    templateUrl: 'modal.error.html',
    styleUrls: ['modal.error.scss']
})
export class DialogErrorComponent {
    static data: any;
    private error: any;
    constructor(public dialogRef: MdDialogRef<DialogErrorComponent>) {
        this.error = DialogErrorComponent.data;
    }
}