import { Component, Output, Input, EventEmitter, AfterViewInit, ViewChild, ElementRef, Renderer2, HostBinding } from '@angular/core';
import { DropFileProvider } from './drop.file.provider';
import { AppConfig } from '../../config/app.config';
@Component({
    moduleId: module.id,
    selector: 'drop-file',
    templateUrl: 'drop.file.html',
    styleUrls: ['drop.file.scss']
})

export class DropFileComponent implements AfterViewInit {
    private id: string;
    private file: any;
    private supportedFileTypes: string[] = ['image/png', 'image/jpeg', 'image/jpg'];
    private allowedExtensions: RegExp = /(\w+)(.jpg|.png|.jpeg)$/;
    private maximumFileSizeInBytes = 1e+6;
    private uploading = false;
    private uploaded = false;
    private imageUrl = '';

    @ViewChild('imageContainer') imageContainer: ElementRef;
    @Output() fileUploaded = new EventEmitter<any>();
    @Input() hasHeaderImg: boolean;
    @Input() initImage: string;

    constructor(private dropFileProvider: DropFileProvider, private render: Renderer2) { }

    ngAfterViewInit() {
        if (this.initImage) {
            this.renderImage(this.initImage);
        }
    }

    renderImage(imageUrl) {
        this.render.setStyle(this.imageContainer.nativeElement, 'background', `url(${AppConfig.API_PATH.images}${imageUrl})`);
        this.render.setStyle(this.imageContainer.nativeElement, 'background-repeat', 'no-repeat');
        this.render.setStyle(this.imageContainer.nativeElement, 'background-size', 'cover');
        this.render.setStyle(this.imageContainer.nativeElement, 'background-position', 'center');
    }

    dragFileAccepted(acceptedFile) {
        if ('target' in acceptedFile) {
            this.uploading = true;
            this.file = acceptedFile.target.files[0];
            this.id = this.file.name.replace(this.allowedExtensions, '$1');
            this.dropFileProvider.uploadFile(this.file, this.id).subscribe(data => {
                this.fileUploaded.emit(data);
                if (this.hasHeaderImg) {
                    this.renderImage(data.url);
                }
                this.uploading = false;
                this.uploaded = true;
            });
        } else {
            const fileReader = new FileReader();
            fileReader.onload = () => {
                this.uploading = true;
                this.file = acceptedFile.file;
                this.id = this.file.name.replace(this.allowedExtensions, '$1');
                this.dropFileProvider.uploadFile(this.file, this.id).subscribe(data => {
                    this.fileUploaded.emit(data);
                    if (this.hasHeaderImg) {
                        this.renderImage(data.url);
                    }
                    this.uploading = false;
                    this.uploaded = true;
                });
            };
            fileReader.readAsDataURL(acceptedFile.file);
        }
    }

}