import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../config/app.config';


@Injectable()
export class DropFileProvider {

    private uploadFileUrl: string;

    constructor() {
        this.uploadFileUrl = AppConfig.API_PATH.images + 'upload';
    }

    uploadFile(file, fileId) {
        let fileToUpload: File;
        if (file !== undefined) {
            fileToUpload = file;
        }
        return new Observable<any>(observer => {
            let formData = new FormData();
            let xhr = new XMLHttpRequest();

            if (fileToUpload) {
                formData.append('image', fileToUpload, fileToUpload.name);
            }
            formData.append('city', AppConfig.city);
            formData.append('id', fileId);

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(JSON.parse(xhr.response));
                    } else {
                        observer.error(JSON.parse(xhr.response));
                    }
                }
            }

            xhr.open('POST', this.uploadFileUrl, true);
            xhr.send(formData);

        });

    }
}