import { NgModule } from '@angular/core';
import { FileDropModule } from '../../directives/file-drop/module/file-drop.module';
import { DropFileComponent } from './drop.file.component';
import { BrowserModule } from '@angular/platform-browser';
import { DropFileProvider } from './drop.file.provider';

@NgModule({
  imports: [
    FileDropModule,
    BrowserModule
  ],
  declarations: [
    DropFileComponent
  ],
  exports: [
    DropFileComponent
  ],
  providers: [DropFileProvider]
})

export class DropFileModule { }
