import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'spinner',
    templateUrl: 'spinner.html',
    styleUrls: ['spinner.component.scss']
})

export class SpinnerComponent {
}