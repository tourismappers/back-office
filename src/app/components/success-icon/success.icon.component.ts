import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'success-icon',
    templateUrl: 'success.icon.component.html',
    styleUrls: ['success.icon.component.scss']
})

export class SuccessIconComponent { }