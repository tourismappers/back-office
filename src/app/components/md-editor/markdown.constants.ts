export const MARKDOWN_STYLES = {
    paragraph: '',
    bold: '**',
    italic: '*',
    h1: '# ',
    h2: '## ',
    h3: '### ',
    h4: '#### ',
    h5: '##### ',
    h6: '###### ',
    endHeaders: ''
};

export const MARKDOWN_LITERALS = {
    paragraph: 'paragraph',
    bold: 'bold',
    italic: 'italic',
    h1: 'h1',
    h2: 'h2',
    h3: 'h3',
    h4: 'h4',
    h5: 'h5',
    h6: 'h6',
    endHeaders: 'endHeaders'
};
