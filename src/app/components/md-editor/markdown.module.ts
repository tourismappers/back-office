import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MarkdownConverterProvider } from './conversion/markdownConverter';
import { MdEditorComponent } from './editor/editor.component';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [BrowserModule,
        FormsModule],
    declarations: [MdEditorComponent],
    exports: [MdEditorComponent],
    providers: [MarkdownConverterProvider]
})

export class MdEditorModule { }