import {
    Component,
    ViewChild,
    Input,
    Output,
    EventEmitter,
    ElementRef,
    AfterContentInit,
    HostListener,
    Renderer
} from '@angular/core';

import { MarkdownConverterProvider } from '../conversion/markdownConverter';
import { MarkdownHelper } from '../helpers/markdown.helper';
import { ToolbarActions } from '../toolbar/toolbar';
import { MARKDOWN_LITERALS } from '../markdown.constants';


@Component({
    moduleId: module.id,
    selector: 'md-editor',
    templateUrl: 'editor.html',
    styleUrls: ['editor.scss'],

})
export class MdEditorComponent implements AfterContentInit {
    private result = '';
    private showToolbar = false;
    @ViewChild('rawMarkdown') rawMarkdown: ElementRef;
    @ViewChild('floatingBar') floatingBar: ElementRef;
    @Output() valueChange = new EventEmitter();
    set value(val) {
        this.result = val;
        this.valueChange.emit(this.result || '');
    }
    @Input()
    get value() {
        return this.result;
    }
    @Input() initValue = '';
    @Input() floatingToolbar = false;
    @Input() placeholder: any;
    constructor(private converter: MarkdownConverterProvider, private render: Renderer) { }

    // Events
    @HostListener('click', ['$event'])
    click($event) {
        if (this.result) {
            console.log(this.floatingBar, $event);
            this.render.setElementStyle(this.floatingBar.nativeElement, 'top', $event.layerY + 20 + 'px');
            this.render.setElementStyle(this.floatingBar.nativeElement, 'left',
                $event.layerX - (this.floatingBar.nativeElement.offsetWidth / 2) + 'px');
            this.showToolbar = !this.showToolbar;
        }
    }
    @HostListener('paste', ['$event'])
    paste($event) {
        console.log($event);
        if ($event.hasOwnProperty('clipboardData')) {
            this.updateValue($event.clipboardData.getData('text/plain'));
        }
    }
    ngAfterContentInit() {
        this.rawMarkdown.nativeElement.value = this.converter.convertToMarkdown(this.initValue);
    }

    setStyle(options) {
        const newValue = ToolbarActions.setStyle({
            element: this.rawMarkdown.nativeElement,
            style: MARKDOWN_LITERALS[options.style],
            tagNumber: options.tagNumber
        });
        this.updateValue(newValue);
    }

    textAreaAutoHeight(obj) {
        obj.style.overflow = 'hidden';
        obj.style.height = 'auto';
        obj.style.height = 25 + obj.scrollHeight + 'px';
    }

    public updateValue(val) {
        this.result = this.converter.convertToHtml(val);
        this.value = this.result;
        if (this.rawMarkdown !== undefined) {
            this.textAreaAutoHeight(this.rawMarkdown.nativeElement);
        }
    }
}