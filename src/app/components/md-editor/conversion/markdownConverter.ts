import * as marked from 'marked';
import { Injectable } from '@angular/core';
import { MARKDOWN_STYLES } from '../markdown.constants';

const paragraph = '\<p\>|\<\/p\>';
const bold = '\<strong\>|\<\/strong\>';
const italic = '\<em\>|\<\/em\>';
const headers = '\<(h[1-6])\\sid\=\"\\.*\"\>';
const endHeaders = '\<\/h[1-6]\>';
const a = '^(https?)(\:\/\/(.*))(\.)(com|es|net|org|uk\.co|co)';

const regExpArray1 = [{ a }, { paragraph }, { bold }, { italic }, { endHeaders }];

@Injectable()
export class MarkdownConverterProvider {
    private md;
    constructor() {
        this.md = marked;
        marked.setOptions({
            renderer: new marked.Renderer(),
            gfm: true,
            tables: true,
            breaks: true,
            pedantic: false,
            sanitize: false,
            smartLists: true,
            smartypants: false
        });
    }

    convertToHtml(markdown: string): string {
        return this.md.parse(markdown);
    }
    convertToMarkdown(text: string) {
        regExpArray1.forEach((i, ind) => {
            const key = Object.keys(i)[0];
            text = text.replace(new RegExp(i[key], 'gm'), MARKDOWN_STYLES[key]);
        });
        text = text.replace(new RegExp(headers, 'gm'), (str, p1) => MARKDOWN_STYLES[p1]);
        return text;
    }
}
