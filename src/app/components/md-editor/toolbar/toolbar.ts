import { MARKDOWN_STYLES } from '../markdown.constants';
import { MarkdownHelper } from '../helpers/markdown.helper';

export class ToolbarActions {

    static setStyle(options): string {
        const { element, style, tagNumber } = options;
        const sel = MarkdownHelper.getInputSelection(element);
        const selection = element.value.substr(sel.start, (sel.end - sel.start));
        const aux = MARKDOWN_STYLES[style] + selection + (tagNumber === 2 ? MARKDOWN_STYLES[style] : '');
        const newValue = element.value.substr(0, sel.start) + aux + element.value.substr(sel.end);
        element.value = newValue;
        sel.start += MARKDOWN_STYLES[style].length;
        sel.end += MARKDOWN_STYLES[style].length;
        MarkdownHelper.setCaretPosition(element, sel);
        return newValue;
    }
}

