import { NgModule } from '@angular/core';
import { SuccessIconComponent } from '../../components/success-icon/success.icon.component';
import { SpinnerComponent } from '../../components/spinner/spinner.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../routes/app.routing';
import { TranslateModule } from 'ng2-translate';
import { FormsModule } from '@angular/forms';
import { MasonryModule } from 'angular2-masonry';
import { DropFileModule } from '../../components/drop-file/drop.file.module';
import { MdEditorModule } from '../../components/md-editor/markdown.module';
import { TaggerModule } from '../../components/tagger/tagger.module';
import { MobileDisplayModule } from '../../components/mobile-display/mobile-display.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdDialogModule } from '@angular/material';
import { MdSnackBarModule } from '@angular/material';









@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        TranslateModule.forRoot(),
        FormsModule,
        MasonryModule,
        DropFileModule,
        MdEditorModule,
        MobileDisplayModule,
        TaggerModule,
        BrowserAnimationsModule,
        MdDialogModule,
        MdSnackBarModule
    ],
    declarations: [
        SuccessIconComponent,
        SpinnerComponent,
    ],
    exports: [
        AppRoutingModule,
        BrowserModule,
        SuccessIconComponent,
        SpinnerComponent,
        FormsModule,
        TranslateModule,
        MasonryModule,
        DropFileModule,
        MdEditorModule,
        MobileDisplayModule,
        TaggerModule,
        BrowserAnimationsModule,
        MdDialogModule,
        MdSnackBarModule
    ]
})
export class SharedModule { }