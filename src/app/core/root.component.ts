import { Component } from '@angular/core';
import 'hammerjs';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    template: '<router-outlet></router-outlet>'
})
export class RootComponent { }
