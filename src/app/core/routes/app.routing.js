"use strict";
var router_1 = require("@angular/router");
var app_component_1 = require("../components/app/app.component");
var home_component_1 = require("../components/home/home.component");
var login_component_1 = require("../components/login/login.component");
var documentation_component_1 = require("../components/documentation/documentation.component");
var app_manager_component_1 = require("../components/app-manager/app.manager.component");

var auth_guard_1 = require("../authentication/auth.guard");
var appRoutes = [
    //main routes
    {
        path: '',
        component: login_component_1.LoginComponent
    },
    {
        path: 'app',
        component: app_component_1.AppComponent,
        canActivate: [auth_guard_1.AuthGuard],
        children: [
            {
                path: 'documentation',
                component: documentation_component_1.DocumentationComponent,
            },
            {
                path: 'appManager',
                component: app_manager_component_1.AppManagerComponent,
                children: [
                    {
                        path: '',
                        component: news_manager_component_1.newsManagerComponent
                    }
                ]
            },
            {
                path: '',
                component: home_component_1.HomeComponent,
            }
        ]
    },
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map