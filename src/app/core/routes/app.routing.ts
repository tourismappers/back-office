import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Main modules
import { RootComponent } from '../../core/root.component';
//import { LoginComponent } from '../../core/login/login-panel/login.component';
import { AppManagerComponent } from '../../core/app-manager/app.manager.component';
import { NewsFormComponent } from '../../modules/news-manager/news-form/news.form.component';

import { ImagesComponent } from '../../modules/images/images.component';

import { NewsPanelComponent } from '../../modules/news-manager/news-panel/news.panel.component';
import { NewsManagerComponent } from '../../modules/news-manager/news.manager.component';
import { NotFoundComponent } from '../../core/not-found/not-found.component';
import { SandboxComponent } from '../../core/sandbox/sandbox/sandbox.component';

// Guards
import { FormGuard } from '../auth/auth.guard';
import { AuthGuard } from '../auth/auth.guard';

const appRoutes: Routes = [
    { path: '', redirectTo: '/app/news/list', pathMatch: 'full' },
    //{ path: 'login', component: LoginComponent },
    {
        path: 'app',
        component: AppManagerComponent,
        // canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'news', pathMatch: 'full' },
            {
                path: 'news',
                component: NewsManagerComponent,
                children: [
                    { path: '', redirectTo: 'list', pathMatch: 'full' },
                    {
                        path: 'list',
                        component: NewsPanelComponent
                    },
                    {
                        canActivate: [FormGuard],
                        path: 'form',
                        component: NewsFormComponent
                    }
                ]
            },
            { path: 'images', component: ImagesComponent },
            { path: 'sandbox', component: SandboxComponent }

        ]
    },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        //AuthGuard, 
        FormGuard
    ]
})
export class AppRoutingModule { }
