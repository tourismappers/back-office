import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'sandbox',
    templateUrl: './sandbox.html',
    styleUrls: ['./sandbox.scss']
})
export class SandboxComponent implements OnInit {
    private content: string;
    tagList = [];
    constructor() { }
    ngOnInit() {
    }

    setTag(tag) {
        this.tagList.push(tag);
    }
}