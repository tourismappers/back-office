import { NgModule } from '@angular/core';

import { SandboxComponent } from './sandbox/sandbox.component';
import { MaterialModule } from '@angular/material';

import { SharedModule } from '../../core/shared/shared.module';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        SandboxComponent
    ]
})

export class SandboxModule { }