import { trigger, state, animate, transition, style } from '@angular/animations';

export const slideLeft =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('slideLeft', [
        // route 'enter' transition
        transition(':enter', [
            // css styles at start of transition
            style({ right: '-100%' }),
            // animation and styles at end of transition
            animate('.3s', style({ right: '0%' }))
        ]),
        transition(':leave', [
            // css styles at start of transition
            style({ right: '0%' }),
            // animation and styles at end of transition
            animate('.3s', style({ right: '-100%' }))
        ]),
    ]);
export const slideRight =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('slideRight', [
        // route 'enter' transition
        transition(':enter', [
            // css styles at start of transition
            style({ left: '-100%' }),
            // animation and styles at end of transition
            animate('.3s', style({ left: '0%' }))
        ]),
        transition(':leave', [
            // css styles at start of transition
            style({ left: '0%' }),
            // animation and styles at end of transition
            animate('.3s', style({ left: '-100%' }))
        ]),
    ]);

