"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../services/auth.service");
<<<<<<< 6dabb03eb4be85fe2e7e50d46c9a8e427999d338
=======
var core_2 = require("@angular/core");
>>>>>>> asdf
//classes
require("./classes");
var LoginComponent = (function () {
    function LoginComponent(auth, router) {
        this.auth = auth;
        this.router = router;
        this.user = {
            username: '',
            password: ''
        };
        this.loginError = {
            code: '',
            message: ''
        };
        this.hideLoginError = true;
        this.tryLogin = false;
    }
    LoginComponent.prototype.onLogin = function () {
        var _this = this;
        this.tryLogin = true;
        console.log('login');
        var promise = this.auth.login(this.user);
        promise
            .then(function (userInfo) {
            _this.tryLogin = false;
            _this.auth.isLoggedIn = true;
            _this.router.navigate(['app']);
        })
            .catch(function (error) {
            _this.loginError = error;
            _this.tryLogin = false;
            _this.hideLoginError = false;
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'login',
        templateUrl: 'login.html',
<<<<<<< 6dabb03eb4be85fe2e7e50d46c9a8e427999d338
        styleUrls: ['login.scss']
=======
        styleUrls: ['login.scss'],
        encapsulation: core_2.ViewEncapsulation.None,
>>>>>>> asdf
    }),
    __metadata("design:paramtypes", [auth_service_1.AuthService, router_1.Router])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map