import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthProvider } from '../../auth/auth.provider';
import { ViewEncapsulation } from '@angular/core';


declare var firebase: any;

@Component({
    moduleId: module.id,
    selector: 'app-login',
    templateUrl: 'login.html',
    styleUrls: ['login.scss']
})
export class LoginComponent {
    private loginSuccess: boolean;
    private tryLogin: boolean;
    constructor(public auth: AuthProvider, private router: Router) {
        this.loginSuccess = false;
        this.tryLogin = false;
        //this.auth.handleAuthentication();
    }

    login(username, password) {
        this.tryLogin = true;
        // this.auth.login(username, password).subscribe(
        //     (data) => {
        //         this.tryLogin = false;
        //         this.loginSuccess = true;
        //         setTimeout(() => {
        //             this.router.navigate(['/app']);
        //         }, 2000);
        //     },
        //     (error) => {
        //         this.tryLogin = false;
        //     });

    }
}
