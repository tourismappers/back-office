import { AuthConfig } from '../../config/auth.config';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

// Avoid name not found warnings

@Injectable()
export class AuthProvider {
    // private userProfile: Object;

    // // Configure Auth0
    // private auth0 = new auth0.WebAuth({
    //     domain: AuthConfig.domain,
    //     clientID: AuthConfig.clientID,
    //     callbackURL: 'http://localhost:4200/app',
    //     responseType: 'token id_token'
    // });

    // constructor(private router: Router) {

    // }

    // public handleAuthentication(): void {
    //     if (this.authenticated()) {
    //         this.router.navigate(['/app']);
    //     }
    //     this.auth0.parseHash((err, authResult) => {
    //         if (authResult && authResult.accessToken && authResult.idToken) {
    //             window.location.hash = '';
    //             localStorage.setItem('access_token', authResult.accessToken);
    //             localStorage.setItem('id_token', authResult.idToken);
    //         } else if (authResult && authResult.error) {
    //             alert('Error: ' + authResult.error);
    //         }
    //     });
    // }

    // public login(username: string, password: string): Observable<any> {
    //     let obs = new Observable((observer) => {
    //         this.auth0.client.login({
    //             realm: 'Username-Password-Authentication',
    //             username,
    //             password
    //         }, (err, authResult) => {
    //             if (err) {
    //                 observer.error(err);
    //             } else if (authResult && authResult.idToken && authResult.accessToken) {
    //                 observer.next();
    //                 this.setUser(authResult);
    //             }

    //         });
    //     })
    //     return obs;
    // }

    // public signup(email, password): void {
    //     this.auth0.redirect.signupAndLogin({
    //         connection: 'Username-Password-Authentication',
    //         email,
    //         password,
    //     }, function (err) {
    //         if (err) {
    //             alert('Error: ' + err.description);
    //         }
    //     });
    // }


    // public authenticated(): boolean {
    //     // Check whether the id_token is expired or not
    //     return tokenNotExpired();
    // }

    // public logout(): void {
    //     // Remove token from localStorage
    //     this.router.navigate(['/login']);
    //     localStorage.removeItem('access_token');
    //     localStorage.removeItem('id_token');
    // }

    // private setUser(authResult): void {
    //     localStorage.setItem('access_token', authResult.accessToken);
    //     localStorage.setItem('id_token', authResult.idToken);
    // }
}