import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
//import { AuthProvider } from './auth.provider';
import { NewsProvider } from '../../modules/news-manager/news.provider'

@Injectable()
export class AuthGuard implements CanActivate {
    // constructor(private auth: AuthProvider, public router: Router) {

    // }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return true;
        // if (this.auth.authenticated()) {
        //     return true;
        // } else {
        //     this.router.navigate(['/']);
        //     return false;
        // }
    }
}


@Injectable()
export class FormGuard implements CanActivate {
    constructor(private news: NewsProvider, public router: Router) {

    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.news.getMode() !== 'list') {
            return true;
        } else {
            this.router.navigate(['/app/news/list']);
            return false;
        }
    }
} 
