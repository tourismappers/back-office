import { Component } from '@angular/core';
import { MenuItem } from './models/menuItem';
import { AuthProvider } from '../auth/auth.provider';
import { AppManagerProvider } from './app.manager.provider';
import { slideRight } from '../animations/slide.animation';

@Component({
    moduleId: module.id,
    selector: 'app-dashboard',
    templateUrl: 'app.manager.html',
    styleUrls: ['app.manager.scss'],
    animations: [slideRight]
})
export class AppManagerComponent {

    private navExpanded: boolean;
    private menuList = [];
    private topBarIcons = [];
    constructor(private AMProvider: AppManagerProvider) {
        this.navExpanded = true;
        AMProvider.AppConfig.modules.map(element => {
            this.menuList.push(MenuItem(element));
        });
        AMProvider.AppConfig.icons.map(element => {
            this.topBarIcons.push(MenuItem(element));
        })
    }

    toogleNavExpanded() {
        this.navExpanded = !this.navExpanded;
    }
    setNavExpandedValue(value) {
        this.navExpanded = value;
    }
}
