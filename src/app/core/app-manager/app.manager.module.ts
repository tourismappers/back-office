import { NgModule } from '@angular/core';
import { AppManagerComponent } from './app.manager.component';
import { AppManagerProvider } from './app.manager.provider';
import { SharedModule } from '../../core/shared/shared.module';
import { MdToolbarModule } from '@angular/material';
import { SandboxComponent } from '../../core/sandbox/sandbox/sandbox.component';
import { ImagesComponent } from '../../modules/images/images.component';

@NgModule({
    imports: [
        //RouterModule.forChild(appManagerRoutes),
        SharedModule,
        MdToolbarModule
    ],
    declarations: [AppManagerComponent],
    providers: [AppManagerProvider]
})

export class AppManagerModule { }