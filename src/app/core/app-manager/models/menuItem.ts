export function MenuItem(spec) {
    const { key, path } = spec;
    return Object.freeze({ key, path });
} 