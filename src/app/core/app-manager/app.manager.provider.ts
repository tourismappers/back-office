import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { AppConfig } from '../../config/app.config';

@Injectable()
export class AppManagerProvider {
    public AppConfig;
    constructor() {
        this.setAppConfig();
    }
    setAppConfig() {
        localStorage.setItem('appConfig', JSON.stringify(AppConfig));
        this.AppConfig = AppConfig;
    }
    getAppConfig() {
        return JSON.parse(localStorage.getItem('appConfig'));
    }
}