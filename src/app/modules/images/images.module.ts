import { NgModule } from '@angular/core';

import { ImagesComponent } from './images.component';
import { SharedModule } from '../../core/shared/shared.module';

// Providers 

import { ImageProvider } from './images.provider';


@NgModule({
    imports: [
        SharedModule
    ],
    providers: [ImageProvider],
    declarations: [
        ImagesComponent
    ]
})

export class ImagesModule { }