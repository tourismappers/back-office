import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ImageProvider } from './images.provider';
import { AppConfig } from '../../config/app.config';
import { slideRight } from '../../core/animations/slide.animation';


@Component({
    moduleId: module.id,
    selector: 'images',
    templateUrl: 'images.html',
    styleUrls: ['images.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [slideRight]
})

export class ImagesComponent implements OnInit {
    private imagesList: Array<any>;
    private imageUrl: string;
    private displayDrop: boolean;

    constructor(private imageProvider: ImageProvider) {
        this.displayDrop = false;
        this.imagesList = [];
        this.imageUrl = AppConfig.API_PATH.images;
    }

    ngOnInit() {
        this.imageProvider.getImagesList().subscribe(data => {
            this.imagesList = data;
        }, error => {
            console.log(error);
        });
    }

    removeImage(ind, id) {
        this.imageProvider.removeImage(id).subscribe(() => {
            this.removeImageFromLocal(ind);
        }, error => {
            console.log(error);
        });
    }

    removeImageFromLocal(ind) {
        this.imagesList.splice(ind, 1);
    }

    fileSucces(data) {
        this.imagesList.unshift(data);
    }
}