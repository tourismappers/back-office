import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../config/app.config';



@Injectable()
export class ImageProvider {

    private searchImageUrl: string;
    private removeImageUrl: string;

    constructor(private http: Http) {
        this.searchImageUrl = AppConfig.API_PATH.images + 'search?city=' + AppConfig.city;
    }

    getImagesList(): Observable<Array<any>> {
        return new Observable(observer => {
            this.http.get(this.searchImageUrl).subscribe(response => {
                observer.next(response.json());
            }, error => {
                observer.error(error.json());
            });
        });
    }

    removeImage(imageId): Observable<any> {
        this.removeImageUrl = AppConfig.API_PATH.images + 'delete/' + imageId;
        return new Observable(observer => {
            this.http.delete(this.removeImageUrl).subscribe(response => {
                observer.next(response.json());
            }, error => {
                observer.error(error.json());
            });
        });
    }
}