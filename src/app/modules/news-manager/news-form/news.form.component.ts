import { Component, OnInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { New } from '../models/news';
import { NewsProvider } from '../news.provider';
import { AppConfig } from '../../../config/app.config';
import { END_POINTS } from '../news.config';
import { MdDialog, MdSnackBar } from '@angular/material';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsManagerComponent } from '../news.manager.component';
import { ImageProvider } from '../../images/images.provider';
import * as _ from 'lodash';
import { GaleryModalComponent } from "../galery/galery.component";
import { slideLeft } from '../../../core/animations/slide.animation';

enum StateButton {
  horizontal = 0,
  back
};


@Component({
  moduleId: module.id,
  selector: 'news-form',
  templateUrl: 'news.form.html',
  styleUrls: ['news.form.scss'],
  animations: [slideLeft]
})

export class NewsFormComponent implements OnInit {
  private mode: string;
  private newObject = {
    _id: '',
    city: AppConfig.city,
    lang: AppConfig.lang,
    longDescription: '',
    categories: [],
    title: '',
    shortDescription: '',
    source: '',
    mainImgUrl: '',
    published: true
  };
  private imageBasePath: string;
  private categoriesAux: Array<any> = [];
  private publishedAt: Date;
  private imagesList: Array<any>;
  private editorReady: boolean;
  private unexpectedError: string;
  private showMore = false;
  private showGallery = false;
  private buttonState = StateButton.horizontal;
  @ViewChild('imageHeader') imageHeader: ElementRef;


  constructor(
    private newsProvider: NewsProvider,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private newsManagerComponent: NewsManagerComponent,
    private snackBar: MdSnackBar,
    private imageProvider: ImageProvider,
    private render: Renderer2,
    public dialog: MdDialog
  ) {
    this.editorReady = true;
    this.mode = this.newsProvider.getMode();
    this.imagesList = [];
    this.imageBasePath = AppConfig.API_PATH.images;
    this.unexpectedError = 'unexpected error';
    // //idiomas disponibles
    translate.addLangs(AppConfig.supportedLang);
    // //idioma por defecto
    translate.setDefaultLang(this.newObject.lang);
    this.publishedAt = new Date();
    // filling select's languages
    // Function to fill select's categories
    this.newsProvider.getCategories().subscribe(
      (categories) => {
        categories.map((category, i) => {
          this.categoriesAux.push(category);
          this.categoriesAux[i].selected = false;
        });

        this.categoriesAux = _.sortBy(this.categoriesAux, AppConfig.lang);

        if (this.mode === 'edit') {
          const auxNew = this.newsProvider.getNewToUpdate();
          this.newObject.title = auxNew.title || '';
          this.newObject.shortDescription = auxNew.shortDescription || '';
          this.newObject.source = auxNew.source || '';
          this.newObject.lang = auxNew.lang;
          this.newObject.longDescription = auxNew.longDescription || '';
          this.newObject.mainImgUrl = auxNew.mainImgUrl;
          this.newObject._id = auxNew._id;
          if (auxNew.categories.length > 0) {
            auxNew.categories.map((category, i) => {
              this.newObject.categories.push(category);
              this.categoriesAux.map((cat, ind) => {
                if (category._id === cat._id) {
                  this.categoriesAux[ind].selected = true;
                }
              });
            });
          }
        }

      }, (error) => {
        this.showSnackBarError(error);
      });
  }


  ngOnInit() {
    this.imageProvider.getImagesList().subscribe(data => {
      this.imagesList = data;
    });
  }

  openSideMenu() {
    this.buttonState = StateButton.back;
    this.showMore = true;
  }
  openGallery() {
    this.buttonState = StateButton.back;
    const dialogRef = this.dialog.open(GaleryModalComponent, {
      data: this.imagesList,
      height: '80vh',
      width: '100%',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.url) {
        this.render.setStyle(this.imageHeader.nativeElement, 'background-image', `url(${this.imageBasePath + result.url})`);
      }
      this.newObject.mainImgUrl = result.url;
    });
    this.showMore = false;
  }
  closeGallery() {
    this.buttonState = StateButton.horizontal;
  }
  closeSideMenu() {
    this.buttonState = StateButton.horizontal;
    this.showMore = false;
  }

  fileSucces(data) {
    this.newObject.mainImgUrl = data.url;
    this.imagesList.unshift(data);
  }

  setImageUrl(image) {
    this.newObject.mainImgUrl = image.url;
  }
  clearImage() {
    this.newObject.mainImgUrl = '';
  }
  pushCategory(category: any): void {
    const categoryToSendIndex = this.newObject.categories.findIndex(function (element) {
      return element._id === category._id;
    });
    if (this.newObject.categories.length > 0) {
      if (categoryToSendIndex === -1) {
        this.newObject.categories.push(category);
      } else {
        this.newObject.categories.splice(categoryToSendIndex, 1);
      }
    } else {
      this.newObject.categories.push(category);
    };

  }
  publish(published) {
    this.newObject.published = published;
    this.saveNew();
  }
  setNew() {
    return New(this.newObject);
  }
  updateNew() {
    this.newsProvider.updateNew(this.newObject._id, New(this.newObject)).subscribe(
      data => {
        this.newsProvider.setMode('list');
        this.router.navigate(['/app/news/list']);
      },
      error => {
        this.showSnackBarError(error);
      });

  }
  saveNew() {
    this.newsProvider.saveNew(this.setNew()).then(
      data => {
        this.newsProvider.setMode('list');

        this.router.navigate(['/app/news/list']);
      },
      error => {
        this.showSnackBarError(error);
      });
  }

  setTags(tag) {
    this.newObject.categories.push(tag);
  }

  showSnackBarError(error) {
    const snackRef = this.snackBar.open(error.message || this.unexpectedError, null, {
      duration: 3000
    });
  }
}
