import { NgModule } from '@angular/core';

// Imports
import { SharedModule } from '../../core/shared/shared.module';

// Component
import { NewsCardComponent } from './news-panel/news-card/news.card.component';
import { MdTabsModule } from '@angular/material';
import { MdChipsModule } from '@angular/material';





// Directives
import { SelectableDirective } from '../../directives/selectable/selectable.directive';

// Views
import { NewsFormComponent } from './news-form/news.form.component';
import { NewsPanelComponent } from './news-panel/news.panel.component';
import { NewsManagerComponent } from './news.manager.component';


// Modals
import { DialogErrorComponent } from '../../components/modals/error/modal.error.component';
import { DialogConfirmComponent } from '../../components/modals/confirm/modal.confirm.component';

// providers
import { NewsProvider } from './news.provider';
import { Routes, RouterModule } from '@angular/router';
import { FormGuard } from '../../core/auth/auth.guard';



// pipes
import { SafeHtmlPipe, FilterPipe } from './news.pipes';
import { GaleryModalComponent } from './galery/galery.component';

const newsRoutes: Routes = [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    {
        path: 'list',
        component: NewsPanelComponent
    },
    {
        canActivate: [FormGuard],
        path: 'form',
        component: NewsFormComponent
    }
];

@NgModule({
    imports: [
        //RouterModule.forChild(newsRoutes),
        SharedModule,
        MdTabsModule,
        MdChipsModule
    ],
    entryComponents: [
        // Modals
        DialogErrorComponent,
        DialogConfirmComponent,
        GaleryModalComponent
    ],
    declarations: [
        // Views
        NewsManagerComponent,
        NewsFormComponent,
        NewsPanelComponent,
        NewsCardComponent,

        // Shared components
        DialogErrorComponent,
        DialogConfirmComponent,

        // Pipes
        SafeHtmlPipe,
        FilterPipe,

        // Directives
        SelectableDirective,

        // Modals
        GaleryModalComponent,
    ],
    providers: [NewsProvider]
})

export class NewsModule { }
