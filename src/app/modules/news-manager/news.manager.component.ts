import { Component } from '@angular/core';

@Component({
    selector: 'news-manager',
    template: '<router-outlet></router-outlet>'
})

export class NewsManagerComponent {
}