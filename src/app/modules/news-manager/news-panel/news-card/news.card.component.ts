import {
    Component,
    Input,
    Output,
    EventEmitter,
    Renderer2,
    AfterViewInit,
    AfterContentInit,
    ViewChild,
    ElementRef,
    HostListener,
} from '@angular/core';
import { New } from '../../models/news';

import { AppConfig } from '../../../../config/app.config';
import { END_POINTS } from '../../news.config';
@Component({
    moduleId: module.id,
    selector: 'news-display',
    templateUrl: 'news.view.component.html',
    styleUrls: ['news.view.component.scss']
})

export class NewsCardComponent implements AfterViewInit, AfterContentInit {
    @Input() news;
    @Output() getAction = new EventEmitter<string>();
    @ViewChild('img') img: ElementRef;

    private actions: Array<any>;
    constructor(private render: Renderer2) {
    }
    sendAction(action) {
        this.getAction.emit(action);
    }
    ngAfterViewInit(): void {
        if (this.news.mainImgUrl !== undefined) {
            this.render.setStyle(
                this.img.nativeElement,
                'background-image',
                `url('${AppConfig.API_PATH.images + this.news.mainImgUrl}')`
            );

        }
    }

    ngAfterContentInit() {
        this.renderActions();
    }
    renderActions() {
        this.actions = [
            { text: 'Edit', hide: this.news.deleted, key: 'edit' },
            {
                text: this.news.deleted ? 'Remove' : 'Delete',
                hide: false, key: this.news.deleted ? 'remove' : 'delete'
            },
            { text: 'Restore', hide: !this.news.deleted, key: 'restore' },
        ];
    }
}
