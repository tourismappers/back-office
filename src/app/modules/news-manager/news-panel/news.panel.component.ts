import { Component } from '@angular/core';
import { NewsProvider } from '../news.provider';
import { New } from '../models/news';
import { MdDialog, MdSnackBar } from '@angular/material';
import { DialogErrorComponent } from '../../../components/modals/error/modal.error.component';
import { DialogConfirmComponent } from '../../../components/modals/confirm/modal.confirm.component';
import { Observable } from 'rxjs/Observable';
import { Router, NavigationExtras } from '@angular/router';
import { NewsManagerComponent } from '../news.manager.component';
import { AppConfig } from '../../../config/app.config';
import * as _ from 'lodash';


@Component({
    moduleId: module.id,
    selector: 'news-list',
    templateUrl: 'news.list.html',
    styleUrls: ['news.list.scss']
})
export class NewsPanelComponent {
    private newsList: Array<any>;
    private deleted: number;
    private isLoading: boolean;
    private emptyNews: boolean;
    private categoriesList: boolean;
    private categories: Array<any>;
    private lang = AppConfig.lang;
    private isFiltered: boolean;
    private filter: string;


    constructor(
        private newsProvider: NewsProvider,
        private snackBar: MdSnackBar,
        private dialog: MdDialog,
        private router: Router,
        private newsManagerComponent: NewsManagerComponent,
    ) {
        this.isLoading = true;
        this.emptyNews = false;
        this.categoriesList = false;
        this.newsList = [];
        this.categories = [];
        this.isFiltered = false;
        this.filter = '';

        this.getCategories();
        this.getNewsList();

    }

    showCategories() {
        this.categoriesList = !this.categoriesList;
    }

    getCategories() {
        this.newsProvider.getCategories().subscribe(
            (categories) => {
                categories.map((category, i) => {
                    this.categories.push(category);
                    this.categories[i].active = false;
                });
                this.categories = _.sortBy(this.categories, AppConfig.lang);
            });
    }

    getNewsList() {
        this.categoriesList = false;
        this.isLoading = true;
        this.newsList.splice(0);
        this.isFiltered = false;
        this.filter = '';
        this.newsProvider.getNewsList().subscribe(
            data => {
                data.map((newItem) => {
                    this.newsList.push(New(newItem));
                });

                this.emptyNews = this.newsList.length > 0 ? false : true;
                this.isLoading = false;
            },
            error => {
                this.emptyNews = true;
                this.isLoading = false;
            });

    }
    localDeleteNew(id: string) {
        this.newsList.map((newItem, index) => {
            if (newItem._id === id) {
                setTimeout(() => {
                    this.deleted = null;
                    this.newsList.splice(index, 1);
                }, 600);
                this.showSnackbar('la noticia ha sido borrada');
            }
        });
    }
    showSnackbar(message, duration = 3000) {
        this.snackBar.open(message, null, {
            duration: duration
        });
    }
    showConfirmModal(question: string) {
        return new Observable<any>(observer => {
            // pasing data to modal controller
            DialogConfirmComponent.data = question;
            // modal options
            const options = {
                disableClose: true
            };
            const dialogRef = this.dialog.open(DialogConfirmComponent, options);
            dialogRef.afterClosed().subscribe(result => {
                observer.next(result);
            });
        });
    }
    showErrorModal(error: any) {
        // pasing data to modal controller
        DialogErrorComponent.data = error;

        // modal options
        const options = {
            disableClose: true
        };
        this.dialog.open(DialogErrorComponent, options);
    }
    removeNew(ind: number) {
        const question = 'Estás seguro de que desea borrar la noticia?';
        this.showConfirmModal(question).subscribe(result => {
            if (result) {
                this.newsProvider.deleteNew(this.newsList[ind]._id).subscribe(
                    data => {
                        this.deleted = ind;
                        this.localDeleteNew(this.newsList[ind]._id);
                    },
                    error => {
                        this.showErrorModal(error);
                    });
            }
        });
    }
    saveAsDraft(ind) {
        const auxNew = Object.assign({}, this.newsList[ind]);
        auxNew.published = false;
        this.newsProvider.updateNew(auxNew._id, auxNew).subscribe(data => {
            this.newsList[ind].published = false;
            this.showSnackbar('saved as draft');
        });
    }
    publishNew(ind) {
        const auxNew = Object.assign({}, this.newsList[ind]);
        auxNew.published = true;
        this.newsProvider.updateNew(auxNew._id, auxNew).subscribe(data => {
            this.newsList[ind].published = true;
            this.showSnackbar('published');
        });
    }
    editNew(ind) {
        this.newsProvider.setNewToUpdate(this.newsList[ind]);
        this.newsProvider.setMode('edit');
        this.router.navigate(['/app/news/form']);
    }

    deleteNew(ind) {
        const auxNew = Object.assign({}, this.newsList[ind]);
        auxNew.deleted = true;
        this.newsProvider.updateNew(auxNew._id, auxNew)
            .subscribe(data => {
                this.localDeleteNew(this.newsList[ind]._id);
                this.newsList[ind].deleted = false;
                this.deleted = ind;
                this.showSnackbar('deleted');

            });
    }

    restore(ind) {
        const auxNew = Object.assign({}, this.newsList[ind]);
        auxNew.deleted = false;
        this.newsProvider.updateNew(auxNew._id, auxNew)
            .subscribe(data => {
                this.localDeleteNew(this.newsList[ind]._id);
                this.newsList[ind].deleted = false;
                this.deleted = ind;
                this.showSnackbar('restored');
            });
    }

    handleActionCard(action, ind) {
        switch (action) {
            case 'edit':
                this.editNew(ind);
                break;
            case 'delete':
                this.deleteNew(ind);
                break;
            case 'remove':
                this.removeNew(ind);
                break;
            case 'draft':
                this.saveAsDraft(ind);
                break;
            case 'restore':
                this.restore(ind);
                break;
            case 'publish':
                this.publishNew(ind);
                break;

        }
    }

    addNew() {
        this.newsProvider.setMode('add');
        this.router.navigate(['/app/news/form']);
    }

    searchForCategory(category) {
        this.categoriesList = false;
        this.isLoading = true;
        this.newsList.splice(0);
        this.isFiltered = true;
        this.filter = category;
        this.newsProvider.searchForCategory(category._id).subscribe(
            data => {
                data.map((newItem) => {
                    this.newsList.push(New(newItem));
                });
                this.emptyNews = this.newsList.length > 0 ? false : true;
                this.isLoading = false;
            },
            error => {
                this.emptyNews = true;
                this.isLoading = false;
            });
    }

}

