import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../config/app.config';
import { END_POINTS } from './news.config';
import { New } from './models/news';

@Injectable()
export class NewsProvider {
    private getCategoriesUrl: string;
    private saveNewUrl: string;
    private getNewsListUrl: string;
    private deleteNewUrl: string;
    private updateNewUrl: string;
    private getDeletedNewsUrl: string;
    private searchUrl: string;
    private files: Array<File>;
    private fileToUpload: File;

    constructor(private http: Http) {
        this.files = [];
        this.getCategoriesUrl = AppConfig.API_PATH.news + END_POINTS.getCategories;
        this.saveNewUrl = AppConfig.API_PATH.news + END_POINTS.saveNew;
        this.getNewsListUrl = AppConfig.API_PATH.news + END_POINTS.getNewsList;
        this.getDeletedNewsUrl = AppConfig.API_PATH.news + END_POINTS.getDeletedNews;
    }

    setNewToUpdate(newItem) {
        localStorage.setItem('itemToEdit', JSON.stringify(newItem));
    }
    getNewToUpdate() {
        return JSON.parse(localStorage.getItem('itemToEdit'));
    }
    setMode(mode) {
        localStorage.setItem('mode', JSON.stringify(mode));
    }
    getMode() {
        return JSON.parse(localStorage.getItem('mode'));
    }

    getCategories(): Observable<Array<any>> {
        return new Observable<Array<any>>((observer) => {
            this.http.get(this.getCategoriesUrl).subscribe(
                data => {
                    observer.next(data.json());
                },
                error => { observer.error(error) });
        });
    }
    saveNew(newObj: any) {
        const config = {
            url: this.saveNewUrl,
            body: newObj
        };
        return new Promise<any>((resolve, reject) => {
            this.http.post(config.url, config.body).subscribe(
                data => {
                    resolve(data.json());
                },
                error => { reject(error.json()); });
        });
    }

    getNewsList() {
        return new Observable<Array<any>>((observer => {
            this.http.get(this.getNewsListUrl).subscribe(
                data => {
                    observer.next(data.json());
                },
                error => {
                    observer.error(error);
                });
        }));
    }

    updateNew(id: string, newItem) {
        this.updateNewUrl = AppConfig.API_PATH.news + END_POINTS.updateNew + id;
        return new Observable<any>((observer => {
            this.http.put(this.updateNewUrl, newItem).subscribe(
                data => {
                    observer.next(data.json());
                },
                error => {
                    observer.error(error);
                });
        }));
    }

    deleteNew(id: string) {
        this.deleteNewUrl = AppConfig.API_PATH.news + END_POINTS.deleteNew + id;
        return new Observable<any>((observer => {
            this.http.delete(this.deleteNewUrl).subscribe(
                data => {
                    observer.next(data.json());
                },
                error => {
                    observer.error(error);
                });
        }));
    }

    searchForCategory(id: any) {
        this.searchUrl = AppConfig.API_PATH.news + END_POINTS.search + '?categoryId=' + id;
        return new Observable<any>((observer => {
            this.http.get(this.searchUrl).subscribe(
                data => {
                    observer.next(data.json());
                },
                error => {
                    observer.error(error);
                });
        }));
    }
    getDeletedNews() {
        return new Observable<any>((observer => {
            this.http.get(this.getDeletedNewsUrl).subscribe(
                data => {
                    observer.next(data.json());
                },
                error => {
                    observer.error(error);
                });
        }));
    }


}
