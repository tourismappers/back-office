import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaleryModalComponent } from './galery.component';

describe('GaleryComponent', () => {
  let component: GaleryModalComponent;
  let fixture: ComponentFixture<GaleryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GaleryModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaleryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
