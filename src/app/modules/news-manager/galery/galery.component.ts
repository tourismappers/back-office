import { Component, Inject } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { MD_DIALOG_DATA } from '@angular/material';
import { AppConfig } from '../../../config/app.config';


@Component({
  moduleId: module.id,
  selector: 'tm-galery',
  templateUrl: 'galery.component.html',
  styleUrls: ['galery.component.scss']
})
export class GaleryModalComponent {
  private imagesList: Array<any>;
  private imageBasePath: string;
  constructor(
    @Inject(MD_DIALOG_DATA) public data: any,
    public dialogRef: MdDialogRef<GaleryModalComponent>
  ) {
    this.imageBasePath = AppConfig.API_PATH.images;
    this.imagesList = data;
  }

  setImageUrl(image) {
    this.dialogRef.close(image);
  }

}
