import { AppConfig } from '../../config/app.config';
export const END_POINTS = {
    getCategories: '/categories',
    saveNew: '/news',
    getNewsList: '/news/' + AppConfig.city + '?lang=' + AppConfig.lang,
    getDeletedNews: '/deletedNews/' + AppConfig.city + '?lang=' + AppConfig.lang,
    search: '/search/news',
    deleteNew: '/news/',
    updateNew: '/news/',
    uploadFile: '/upload',
    getImage: '/search?'
};
