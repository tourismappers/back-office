export function New(spec) {
    const {
        _id,
        city,
        lang,
        title,
        publishedAt,
        shortDescription,
        categories,
        mainImgPath,
        mainImgUrl,
        longDescription,
        source,
        published,
        deleted
            } = spec;
    return Object.seal({
        _id,
        city,
        lang,
        title,
        publishedAt,
        shortDescription,
        categories,
        mainImgPath,
        mainImgUrl,
        longDescription,
        source,
        published,
        deleted
    });
}
export function Category(spec) {
    const {
        _id,
        es,
        ca,
        en,
        selected
            } = spec;
    return Object.seal({
        _id,
        es,
        ca,
        en,
        selected
    });
}

